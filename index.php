<?php
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("Shaun");

    echo "Nama Hewan: ".$sheep->name."<br>"; // "shaun"
    echo "Jumlah Kaki : ".$sheep->legs."<br>"; // 2
    echo "Berdarah Dingin : ".$sheep->cold_blooded."<br><br>"; // false

    $sungokong = new Ape("Kera sakti");
    echo "Nama : ".$sungokong->name."<br>";
    echo "Jumlah Kaki : ".$sungokong->legs."<br>";
    echo "Suara : "; $sungokong->yell(); echo "<br><br>"; // "Auooo"

    $kodok = new Frog("buduk");
    echo "Nama : ".$kodok->name."<br>";
    echo "Jumlah Kaki : ".$kodok->legs."<br>";
    echo "Suara : "; $kodok->jump(); // "hop hop"

?>